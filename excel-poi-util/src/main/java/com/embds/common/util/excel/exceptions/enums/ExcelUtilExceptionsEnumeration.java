package com.embds.common.util.excel.exceptions.enums;

import com.embds.common.util.domain.exception.IErrorElement;

/**
 *
 * @author  Eduardo BORGES DA SILVA
 *
 */
public enum ExcelUtilExceptionsEnumeration implements IErrorElement {

	nullExcelTemplate("NULL_EXCEL_TEMPLATE", "The excel template can't be null"), invalidSheetIndex(
			"INVALID_EXCEL_SHEET_INDEX", "The template doesn't contain a sheet with index {{0}}"), unexpectedError(
					"EXCEL_UNEXPECTED_ERROR", "Unexpected error creating excel file : {{0}}"), InvalidTemplateFormat(
							"EXCEL_INVALID_TEMPLATE_FORMAT",
							"The format of the excel template isn't valid!"), EncriptedTemplate("ENCRIPTED_EXCEL_ERROR",
									"This template is encripted and can't be opened! \n Error details : {{0}}"), InvalidClass(
											"EXCEL_CLASS_NOT_SUPPORTED",
											"This class type isn't not yet managed: {{0}}"), InvalidNumericClass(
													"EXCEL_NUMERIC_CLASS_NOT_SUPPORTED",
													"This numeric class type isn't not yet managed: {{0}}"), NullExcelCell(
															"NULL_EXCEL_CELL",
															"The excel cell can't be nul!!"), NullWorkBook(
																	"NULL_WORKBOOK",
																	"The workbok can't be null!"), NullCellCoordinates(
																			"NULL_CELL_COORDINATES",
																			"The coordinates can't are null!"), NullCellCoordinatesValues(
																					"NULL_CELL_COORDINATES_VALUES",
																					"The cell coordinates values can't are null!");

	private final String SECTION_CODE = "EXCEL.UTIL.";

	private String errorCode;
	private String errorMessage;

	private ExcelUtilExceptionsEnumeration(final String errorCode, final String errorMessage) {
		this.errorCode = SECTION_CODE + errorCode;
		this.errorMessage = errorMessage;
	}

	@Override
	public String getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String getErrorMessage() {
		return this.errorMessage;
	}

}
