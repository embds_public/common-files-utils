package com.embds.common.util.excel.beans;

/**
 *
 * Class for determinate the coordinate for a cell in a excel workbook
 *
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class CellCoordinates {

	private final int sheetIndex;
	private final int rowIndex;
	private final int columnIndex;

	public CellCoordinates(final int sheetIndex, final int rowIndex, final int columnIndex) {
		this.sheetIndex = sheetIndex;
		this.rowIndex = rowIndex;
		this.columnIndex = columnIndex;
	}

	/**
	 * @return the sheetIndex
	 */
	public int getSheetIndex() {
		return sheetIndex;
	}

	/**
	 * @return the rowIndex
	 */
	public int getRowIndex() {
		return rowIndex;
	}

	/**
	 * @return the columnIndex
	 */
	public int getColumnIndex() {
		return columnIndex;
	}

}
