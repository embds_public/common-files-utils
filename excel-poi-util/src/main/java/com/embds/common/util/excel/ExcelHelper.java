package com.embds.common.util.excel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.embds.common.util.excel.beans.CellCoordinates;
import com.embds.common.util.excel.beans.CellValueInsertion;
import com.embds.common.util.excel.exceptions.ExcelUtilException;
import com.embds.common.util.excel.exceptions.enums.ExcelUtilExceptionsEnumeration;

/**
 * This class contains some helpful methods to create excel files using POI framework
 *
 * @author Eduardo BORGES DA SILVA
 *
 */
public class ExcelHelper {

	private static final Logger LOG = Logger.getLogger(ExcelHelper.class.getName());

	/**
	 *  This function should insert in a excel file the data (dataToInsert) starting on line lineToStart
	 * <br>- This function clones the style of the lineToStart and the line immediately
	 * before to this row and apply this styles alternately on pairs and no pairs rows if they have styles to clone
	 * <p>NOTE: the styles are cloned (if exists) by column cell; If style exists for first line, and none style exists for second line, then the same style is used on all lines
	 *
	 * @param templateExcel The template excel where data should be inserted
	 * @param sheetIndexToAddData The sheet where the data should be inserted
	 * @param lineToStart The line where the insertion should start
	 * @param dataToinsert The List of List Objects containing the values to insert on columns
	 * @return
	 * @throws ExcelUtilException
	 * @throws TechnicalException
	 */
	protected byte[] addValuesObjectToExcelFile(final byte[] templateExcel, final int sheetIndexToAddData,
			final int lineToStart, final List<List<Object>> dataToinsert) throws ExcelUtilException {
		LOG.info("addValuesObjectToExcelFile - start of creation excel file");
		if (templateExcel == null) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.nullExcelTemplate);
		}
		Workbook workbook = null;
		try {
			workbook = createWorkbook(templateExcel);
			return addValuesObjectToExcelFile(workbook, sheetIndexToAddData, lineToStart, dataToinsert);
		} catch (final EncryptedDocumentException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.EncriptedTemplate, e);
		}
	}



	/**
	 * Create a WorkBook by byte[]
	 * @param templateExcel The byte[] File
	 * @return
	 * @throws ExcelUtilException
	 */
	protected Workbook createWorkbook(final byte[] templateExcel) throws ExcelUtilException {
		try {
			return WorkbookFactory.create(new ByteArrayInputStream(templateExcel));
		} catch (final IOException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.unexpectedError, e, e.toString());
		} catch (final InvalidFormatException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.InvalidTemplateFormat, e);
		} catch (final EncryptedDocumentException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.EncriptedTemplate, e);
		}
	}


	/**
	 * Return the exact Cell for the specified cell coordinates
	 * @param workbook
	 * @param coordinates
	 * @return
	 * @throws ExcelUtilException
	 */
	protected Cell getCellByIndex(final Workbook workbook, final CellCoordinates coordinates)
			throws ExcelUtilException {
		if (workbook == null) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.NullWorkBook);
		}
		if (coordinates == null) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.NullCellCoordinates);
		}
		return workbook.getSheetAt(coordinates.getSheetIndex()).getRow(coordinates.getRowIndex())
				.getCell(coordinates.getColumnIndex());
	}

	/**
	 * Add a value do the designated cell coordinates
	 * @param workbook
	 * @param coordinates
	 * @param value
	 * @throws ExcelUtilException
	 */
	protected void addValueToCell(final Workbook workbook, final CellCoordinates coordinates, final Object value)
			throws ExcelUtilException {
		addValueToCell(getCellByIndex(workbook, coordinates), value);
	}

	/**
	 * Add the corresponding value for each CellValueInsertion object on the especified coordinates
	 * @param workbook
	 * @param valuesDetailsToAdd
	 * @throws ExcelUtilException
	 */
	protected void addValuesToCell(final Workbook workbook, final List<CellValueInsertion> valuesDetailsToAdd)
			throws ExcelUtilException {
		if (valuesDetailsToAdd == null) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.NullCellCoordinatesValues);
		}
		for (final CellValueInsertion valueDetails : valuesDetailsToAdd) {
			addValueToCell(workbook, valueDetails.getCoordinates(), valueDetails.getValue());
		}
	}

	/**
	 * This function should insert in a excel file the data (dataToInsert) starting on line lineToStart
	 * <br>- This function clones the style of the lineToStart and the line immediately
	 * before to this row and apply this styles alternately on pairs and no pairs rows if they have styles to clone
	 * <p>NOTE: the styles are cloned (if exists) by column cell; If style exists for first line, and none style exists for second line, then the same style is used on all lines
	 *
	 * @param workbook
	 * @param sheetIndexToAddData
	 * @param lineToStart
	 * @param dataToinsert
	 * @return
	 * @throws ExcelUtilException
	 */
	protected byte[] addValuesObjectToExcelFile(final Workbook workbook, final int sheetIndexToAddData,
			final int lineToStart, final List<List<Object>> dataToinsert) throws ExcelUtilException {
		return saveToByteArray(addValuesObjectToWorkbook(workbook, sheetIndexToAddData, lineToStart, dataToinsert));
	}

	/**
	 * This function should insert in a excel file the data (dataToInsert) starting on line lineToStart
	 * <br>- This function clones the style of the lineToStart and the line immediately
	 * before to this row and apply this styles alternately on pairs and no pairs rows if they have styles to clone
	 * <p>NOTE: the styles are cloned (if exists) by column cell; If style exists for first line, and none style exists for second line, then the same style is used on all lines
	 * @param workbook
	 * @param sheetIndexToAddData
	 * @param lineToStart
	 * @param dataToinsert
	 * @return a Workbook - use it if needed!!!
	 * @throws ExcelUtilException
	 */
	protected Workbook addValuesObjectToWorkbook(final Workbook workbook, final int sheetIndexToAddData,
			final int lineToStart, final List<List<Object>> dataToinsert) throws ExcelUtilException {
		LOG.info("addValuesObjectToExcelFile - start of creation excel file");
		try {

			Sheet sheet = null;
			try {
				sheet = workbook.getSheetAt(sheetIndexToAddData);
			} catch (final IllegalArgumentException e) {
				throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.invalidSheetIndex,
						String.valueOf(sheetIndexToAddData));
			}
			int indexRow = lineToStart;
			int indexCell;
			final Row sourcePairRow = sheet.getRow(lineToStart);
			final Row sourceUnpairRow = sheet.getRow(lineToStart + 1);

			/**
			 * Save the CellStyle for each column index of the pair row
			 */
			final HashMap<Integer, CellStyle> stylesPairRow = new HashMap<Integer, CellStyle>();
			// Get styles of each cell
			if (sourcePairRow != null) {
				for (final Cell cell : sourcePairRow) {
					stylesPairRow.put(cell.getColumnIndex(), cell.getCellStyle());
				}
			}

			/**
			 * Save the CellStyle for each column index of the unpair row
			 */
			final HashMap<Integer, CellStyle> stylesUnpairRow = new HashMap<Integer, CellStyle>();
			// Get styles of each cell
			if (sourceUnpairRow != null) {
				for (final Cell cell : sourceUnpairRow) {
					stylesUnpairRow.put(cell.getColumnIndex(), cell.getCellStyle());
				}
			} else {
				// if not second line defined, copy same style of first
				stylesUnpairRow.putAll(stylesPairRow);
			}

			for (final List<Object> row : dataToinsert) {
				final Row sheetRow = sheet.createRow(indexRow++);
				indexCell = 0;
				for (final Object value : row) {
					final Cell cell = sheetRow.createCell(indexCell);
					addValueToCell(cell, value);
					cell.setCellStyle(
							indexRow % 2 == 0 ? stylesPairRow.get(indexCell) : stylesUnpairRow.get(indexCell));
					indexCell++;
				}
			}
		} catch (final EncryptedDocumentException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.EncriptedTemplate, e);
		}
		LOG.info("addValuesObjectToExcelFile - end of creation excel file");
		return workbook;
	}

	/**
	 * This function create a byte[] from the give workbook
	 * @param workbook
	 * @return
	 * @throws ExcelUtilException
	 */
	protected byte[] saveToByteArray(final Workbook workbook) throws ExcelUtilException {
		LOG.info("addValuesObjectToExcelFile - start of creation excel file");
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			workbook.write(bos);
			bos.close();
		} catch (final IOException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.unexpectedError, e);
		} catch (final EncryptedDocumentException e) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.EncriptedTemplate, e);
		}
		LOG.info("addValuesObjectToExcelFile - end of creation excel file");
		return bos.toByteArray();
	}

	/**
	 * Giving a Cell and an Object inserts the value on the cell on the right type
	 * <h1>ATTENTION</h1>
	 * Only Double, Long and String types are managed! You should implement all other types!
	 * @param cell
	 * @param valueToInsert
	 * @throws ExcelUtilException
	 * @throws InvalidClassException
	 */
	protected void addValueToCell(final Cell cell, final Object valueToInsert) throws ExcelUtilException {
		if (cell == null) {
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.NullExcelCell);
		}
		//TODO add other types management (date, etc...)
		if (valueToInsert != null) {
			if (valueToInsert instanceof Double || valueToInsert instanceof Long) {
				addNumericValueToCell(cell, valueToInsert);
			} else if (valueToInsert instanceof String) {
				cell.setCellValue(valueToInsert.toString());
			} else {
				throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.InvalidClass, Object.class.getName());
			}
		}
	}

	/**
	 * Add the valueToInsert on the defined cell
	 *
	 * @param cell
	 * @param valueToInsert
	 * @throws InvalidClassException
	 * @throws ExcelUtilException
	 */
	private void addNumericValueToCell(final Cell cell, final Object valueToInsert) throws ExcelUtilException {
		if (valueToInsert instanceof Double) {
			cell.setCellValue((Double) valueToInsert);
		} else if (valueToInsert instanceof Long) {
			cell.setCellValue(((Long) valueToInsert).doubleValue());
		} else {
			LOG.warning("The class '" + Object.class.getName()
					+ "' isn't not yet managed on 'addNumericValutToCell' function");
			throw new ExcelUtilException(ExcelUtilExceptionsEnumeration.InvalidNumericClass,
					Object.class.getName());
		}
	}

	/**
	 * Given a workbook, get the value of the sheet index, line number and column number on string format
	 *
	 * @param workbook
	 * @param sheetIndex
	 * @param lineNumber
	 * @param cellNumber
	 * @return
	 */
	protected String getCellValueString(final Workbook workbook, final int sheetIndex, final int lineNumber,
			final int cellNumber) {
		return workbook.getSheetAt(sheetIndex).getRow(lineNumber).getCell(cellNumber).getStringCellValue();
	}

	/**
	 * Given a workbook, get value of sheet index, line number and column number as Object
	 *
	 * @param workbook
	 * @param sheetIndex
	 * @param lineNumber
	 * @param cellNumber
	 * @return
	 */
	protected Object getCellValue(final Workbook workbook, final int sheetIndex, final int lineNumber,
			final int cellNumber) {
		return getValueOfCell(workbook.getSheetAt(sheetIndex).getRow(lineNumber).getCell(cellNumber));
	}

	/**
	 * Get value of the give cell as Object
	 *
	 * @param cell
	 * @return
	 */
	protected Object getCellValue(final Cell cell) {
		return getValueOfCell(cell);
	}

	/**
	 * Given a workbook, get value of sheet index, line number and column number as Object. If the value is null, an empty String is returned
	 *
	 * @param workbook
	 * @param sheetIndex
	 * @param lineNumber
	 * @param cellNumber
	 * @return
	 */
	protected Object getCellValueNoNull(final Workbook workbook, final int sheetIndex, final int lineNumber,
			final int cellNumber) {
		return getObjectNoNull(getValueOfCell(workbook.getSheetAt(sheetIndex).getRow(lineNumber).getCell(cellNumber)));
	}

	/**
	 * Get value of the give cell as Object. If the value is null, an empty String is returned
	 *
	 * @param cell
	 * @return
	 */
	protected Object getCellValueNoNull(final Cell cell) {
		return getObjectNoNull(getValueOfCell(cell));
	}

	// TODO add code to manage all excel types
	/**
	 * Get value of the give cell on appropriate type according to cell type
	 *
	 * @param cell
	 * @return
	 */
	private Object getValueOfCell(final Cell cell) {
		Object valueOfCell = null;
		if (cell != null) {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				valueOfCell = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					valueOfCell = cell.getDateCellValue();
				} else {
					valueOfCell = cell.getNumericCellValue();
				}
				break;
			default:
				break;
			}
		}
		return valueOfCell;
	}

	/**
	 * Given an object, return the Object if no null or an empty string if null
	 *
	 * @param object
	 * @return
	 */
	private Object getObjectNoNull(final Object object) {
		if (object == null) {
			return "";
		}
		return object;
	}

}
