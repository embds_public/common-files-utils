package com.embds.common.util.excel.exceptions;

import com.embds.common.util.domain.exception.CommonUtilException;
import com.embds.common.util.domain.exception.IErrorElement;

public class ExcelUtilException extends CommonUtilException {

	private static final long serialVersionUID = -9058587781541798851L;

	public ExcelUtilException(final IErrorElement error, final Exception exception) {
		super(error, exception);
	}

	public ExcelUtilException(final IErrorElement error, final Object... data) {
		super(error, data);
	}

	public ExcelUtilException(final IErrorElement error, final Exception exception, final Object... data) {
		super(error, exception, data);
	}

	public ExcelUtilException(final IErrorElement error) {
		super(error);
	}

}
