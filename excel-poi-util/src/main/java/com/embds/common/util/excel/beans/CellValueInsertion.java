package com.embds.common.util.excel.beans;

/**
 *
 * This class should be used for store information of values to add for a cell
 *
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class CellValueInsertion {

	private final CellCoordinates coordinates;
	private final Object value;

	/**
	 * Create a bean with coordinates and value to a cell
	 * @param coordinates The cell coordinates
	 * @param value The value
	 */
	public CellValueInsertion(final CellCoordinates coordinates, final Object value) {
		super();
		this.coordinates = coordinates;
		this.value = value;
	}

	/**
	 * @return the coordinates
	 */
	public CellCoordinates getCoordinates() {
		return coordinates;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

}
