# Common-files-utils

Contains different maven modules projects to help on manipulation of excel and PDF files with Apache POI and Apache PDFBOX

# Modules Description
This project has two main modules to manipulate Excel files (using Apache POI) and Pdf files (using Apache PDFBOX)

# HowTo
- To manipulate PDF files, just extends the PdfHelper class in your own class and use all the existing methods to create a PDF file

- To manipulate Excel files, extends the ExcelHelper class in your own class and use all the existing methods to create a Excel file

- Excel test helper : use the class ExcelAssertion has explained on the javadoc class

# TODO (will be done)
This project has to be enriched with more complete methods.
Methods to read excel files and etc should be create!

I'm waiting for your methods sugestions! ;)

# Tier dependencies
PDFBOX => org.apache.pdfbox:pdfbox:2.0.1
POI => org.apache.poi:poi-ooxml:3.14

# Contact me
If you want contact me, please send me a mail to embds@outlook.com with subject starting by "GIT-common files utils : "

Thank you!
