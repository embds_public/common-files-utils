package com.embds.common.util.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.embds.common.util.pdf.beans.Font;
import com.embds.common.util.pdf.beans.TableBodyColumnDetails;
import com.embds.common.util.pdf.beans.TableHeaderColumnDetails;
import com.embds.common.util.pdf.boxable.BaseTable;
import com.embds.common.util.pdf.boxable.Row;
import com.embds.common.util.pdf.exceptions.PdfUtilException;
import com.embds.common.util.pdf.exceptions.enums.PdfUtilExceptionsEnumeration;

/**
 * This class implements the basics methods for create PDF exports!
 *
 * <p>NOTE: The boxable classes are been imported from com.github.dhorions:boxable project!!
 *
 * @see <a href="https://stackoverflow.com/questions/28059563/how-to-create-table-using-apache-pdfbox">Creating Table using Apache PDF Box</a>
 * @see <a href="https://github.com/dhorions/boxable/tree/master/src/main/java/be/quodlibet/boxable">Boxable project - Git hub</a>
 *
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class PdfHelper {

	// Static rotation values
	private static final int ROTATION_90 = 90;
	private static final int ROTATION_270 = 270;

	private static final float DEFAULT_TABLE_ROW_HEIGHT = 12;

	private static final int HEADER_MARGING = 20;
	private static final int FOOTER_MARGING = HEADER_MARGING;

	private static final float TABLE_BOTTOM_MARGIN = 30;
	private static final float TABLE_BORDER_MARGIN = 20;

	private final static PDType1Font FONT = PDType1Font.HELVETICA;
	private final static float FOOTER_FONT_SIZE = 8f;

	private final static String FOOTER_DATE_FORMAT = "dd/MM/yyyy";
	private final static String FOOTER_SEPARATOR_PAGE = " / ";

	/**
	 * This method add a default footer to all document pages
	 * Insert the Date of the day on the left and the pages report on the right
	 * @param doc
	 * @throws IOException
	 */
	protected static void addDocumentDefaultFooter(final PDDocument doc) throws IOException {
		int currentPage = 1;
		String dateToInsert = new SimpleDateFormat(FOOTER_DATE_FORMAT).format(new Date());
		for (final PDPage page : doc.getDocumentCatalog().getPages()) {
			// calculate to center of the page
			final PDRectangle pageSize = page.getMediaBox();
			final int rotation = page.getRotation();
			final boolean rotate = rotation == ROTATION_90 || rotation == ROTATION_270;
			final float pageWidth = rotate ? pageSize.getHeight() : pageSize.getWidth();
			final float pageHeight = rotate ? pageSize.getWidth() : pageSize.getHeight();

			final PDPageContentStream contentStreamFooter = new PDPageContentStream(doc, page, AppendMode.APPEND, true,
					true);
			addFooter(contentStreamFooter, pageHeight, pageWidth,dateToInsert, currentPage++, doc.getNumberOfPages());
		}
	}
	

	/**
	 * This function add footer to current stream page
	 * @param contentStreamFooter
	 * @param pageHeight
	 * @param pageWidth
	 * @param currentPage
	 * @param totalPages
	 * @throws IOException
	 */
	private static void addFooter(final PDPageContentStream contentStreamFooter, final float pageHeight,
			final float pageWidth, String dateToInsert, final int currentPage, final int totalPages) throws IOException {
		// Add footer line
		contentStreamFooter.moveTo(FOOTER_MARGING, 20);
		contentStreamFooter.lineTo(pageWidth - FOOTER_MARGING, 20);
		contentStreamFooter.stroke();

		final String footerDate = dateToInsert;
		contentStreamFooter.beginText();
		contentStreamFooter.newLineAtOffset(FOOTER_MARGING + 5, 8);
		contentStreamFooter.setFont(FONT, FOOTER_FONT_SIZE);
		contentStreamFooter.showText(footerDate);
		contentStreamFooter.endText();

		final String footerRightInfo = currentPage + FOOTER_SEPARATOR_PAGE + totalPages;
		final float startXFooterRight = pageWidth - FOOTER_MARGING - 10
				- (FONT.getStringWidth(footerRightInfo) * FOOTER_FONT_SIZE / 1000f);
		contentStreamFooter.beginText();
		contentStreamFooter.newLineAtOffset(startXFooterRight, 8);
		contentStreamFooter.setFont(FONT, FOOTER_FONT_SIZE);
		contentStreamFooter.showText(footerRightInfo);
		contentStreamFooter.endText();

		contentStreamFooter.close();
	}

	// Table

	/**
	 * Create a PDF BaseTable and return it
	 * @param doc The PDF Document
	 * @param yStartTable Y Point where BaseTable should start
	 * @param yStartTableNewPage Y Point where BaseTable should start on each new PDF Page
	 * @param borderMargin The Border Margin
	 * @param bottomMargin The Bottom border Margin
	 * @return
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected BaseTable createTableOnDocument(final PDDocument doc, final float yStartTable,
			final float yStartTableNewPage, final float borderMargin, final float bottomMargin)
			throws IOException, PdfUtilException {
		return createTable(doc, yStartTable, yStartTableNewPage, borderMargin, bottomMargin, null);
	}

	/**
	 * Create a PDF BaseTable with some default vars and return it
	 * @param doc The PDF Document
	 * @param yStartTable Y Point where BaseTable should start
	 * @param yStartTableNewPage Y Point where BaseTable should start on each new PDF Page
	 * @return
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected BaseTable createTableOnDocument(final PDDocument doc, final float yStartTable,
			final float yStartTableNewPage) throws IOException, PdfUtilException {
		return createTable(doc, yStartTable, yStartTableNewPage, TABLE_BORDER_MARGIN, TABLE_BOTTOM_MARGIN, null);
	}

	/**
	 * Create a PDF BaseTable with some default vars and return it
	 * @param doc The PDF Document
	 * @param yStartTable Y Point where BaseTable should start
	 * @param yStartTableNewPage Y Point where BaseTable should start on each new PDF Page
	 * @param TableWidth The Table width
	 * @return
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected BaseTable createTableOnDocument(final PDDocument doc, final float yStartTable,
			final float yStartTableNewPage, final float TableWidth) throws IOException, PdfUtilException {
		return createTable(doc, yStartTable, yStartTableNewPage, TABLE_BORDER_MARGIN, TABLE_BOTTOM_MARGIN, TableWidth);
	}


	/**
	 * Create a BaseTable
	 * @param doc The PDF Document
	 * @param yStartTable Y Point where BaseTable should start
	 * @param yStartTableNewPage Y Point where BaseTable should start on each new PDF Page
	 * @param borderMargin The Border Margin
	 * @param bottomMargin The Bottom border Margin
	 * @param tableWidth The Table width => if null, calculate it for use complete page width (without the border size)
	 * @return
	 * @throws PdfUtilException
	 * @throws IOException
	 */
	private BaseTable createTable(final PDDocument doc, final float yStartTable, final float yStartTableNewPage,
			final float borderMargin, final float bottomMargin, Float tableWidth)
			throws PdfUtilException, IOException {
		final PDPage page = addNewPageA4(doc);
		final float pageHeight = page.getMediaBox().getHeight();
		final float yStart = pageHeight - yStartTable;
		final float yStartNewPage = pageHeight - yStartTableNewPage;
		if (tableWidth == null) {
			tableWidth = page.getMediaBox().getWidth() - 2 * borderMargin;
		}
		// Initialize table
		return new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, borderMargin, doc, page, true, true);
	}

	/**
	 * Add a new A4 page to the current PDF Document
	 * @param doc
	 * @return
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected PDPage addNewPageA4(final PDDocument doc) throws IOException, PdfUtilException {
		if (doc == null) {
			throw new PdfUtilException(PdfUtilExceptionsEnumeration.nullPdfDocument);
		}
		final PDPage page = new PDPage();
		page.setMediaBox(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
		doc.addPage(page);
		return page;
	}

	/**
	 * This method create a header table with headerDetails values containing a row
	 * by each element of the first List and one column by each element of the List element
	 *
	 * @param table
	 * @param headerDetails
	 */
	protected void createTableHeader(final BaseTable table, final List<List<TableHeaderColumnDetails>> headerDetails) {
		// Iterate for add each header line
		for (final List<TableHeaderColumnDetails> headerColumnDetails : headerDetails) {
			createRowHeader(table, headerColumnDetails);
		}
	}

	/**
	 * Add all column details for a header row
	 * @param table
	 * @param headerColumnDetails
	 */
	private void createRowHeader(final BaseTable table, final List<TableHeaderColumnDetails> headerColumnDetails) {
		final Row<PDPage> headerRow = table.createRow(16f);
		headerRow.setHeaderRow(true);
		// Iterate for add each column header for current line
		for (final TableHeaderColumnDetails column : headerColumnDetails) {
			headerRow.createCellFormatHeader(column.getColumnSize(), column.getColumnContent(), column.getAlign(),
					column.getTextColor(), column.getFillColor(), column.getFont().getFontStyle(),
					column.getPaddingTop(), column.getCellBorders());
		}
		table.addHeaderRow(headerRow);
	}

	/**
	 * Giving a Header and a Body content create, add and persist the data to a PDF Table
	 * @param table
	 * @param headerDetails
	 * @param bodyDetails
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected void addTableContent(final BaseTable table, final List<List<TableHeaderColumnDetails>> headerDetails,
			final List<List<TableBodyColumnDetails>> bodyDetails) throws IOException, PdfUtilException {
		createTableHeader(table, headerDetails);
		addTableBodyContentAndPersistTable(table, bodyDetails);
	}

	/**
	 * Add text to the First Page of The doc PDF
	 * @param doc The PDF Document
	 * @param borderMargin The border margin (x)
	 * @param heightMargin The Top margin (y)
	 * @param FONT The Font
	 * @param textToDisplay The text do be display
	 * @throws IOException
	 */
	protected void addTextLineToFirstPage(final PDDocument doc, final float borderMargin, final float heightMargin,
			final Font font, final String textToDisplay) throws IOException {
		addTextLineToPage(doc, 0, borderMargin, heightMargin, font, textToDisplay);
	}

	/**
	 * Add text to the specified pageIndex PDF Document
	 * @param doc The PDF Document
	 * @param pageIndex The index of page where text should be added
	 * @param borderMargin The border margin (x)
	 * @param heightMargin The Top margin (y)
	 * @param FONT The Font
	 * @param textToDisplay The text do be display
	 * @throws IOException
	 */
	protected void addTextLineToPage(final PDDocument doc, final int pageIndex, final float borderMargin,
			final float heightMargin, final Font font, final String textToDisplay) throws IOException {
		final PDPage firstPage = doc.getPage(pageIndex);
		final PDPageContentStream contentStreamHeader = new PDPageContentStream(doc, firstPage, AppendMode.APPEND, true,
				true);
		contentStreamHeader.beginText();
		contentStreamHeader.newLineAtOffset(borderMargin, gerPageHeight(firstPage) - heightMargin);
		contentStreamHeader.setFont(font.getFontStyle(), font.getFontSize());
		contentStreamHeader.showText(textToDisplay);
		contentStreamHeader.endText();
		contentStreamHeader.close();
	}

	/**
	 * get the page Width
	 * @param page The page width
	 * @return
	 */
	protected float getPageWidth(final PDPage page) {
		final PDRectangle pageSize = page.getMediaBox();
		final int rotation = page.getRotation();
		final boolean rotate = rotation == 90 || rotation == 270;
		return rotate ? pageSize.getHeight() : pageSize.getWidth();
	}

	/**
	 * get the page Height
	 * @param page the page height
	 * @return
	 */
	protected float gerPageHeight(final PDPage page) {
		final PDRectangle pageSize = page.getMediaBox();
		final int rotation = page.getRotation();
		final boolean rotate = rotation == 90 || rotation == 270;
		return rotate ? pageSize.getWidth() : pageSize.getHeight();
	}

	/**
	 * This function add the body content and persist all table content!
	 *
	 * @param table
	 * @param bodyDetails
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected void addTableBodyContent(final BaseTable table, final List<List<TableBodyColumnDetails>> bodyDetails)
			throws IOException, PdfUtilException {
		for (final List<TableBodyColumnDetails> rowDetails : bodyDetails) {
			addTableRowContent(table, rowDetails);
		}
	}
	
	/**
	 * This function add the body content and persist all table content!
	 
	 * ATTENTION : By calling this method, the BaseTable will be been draw() !!!
	 *
	 * @param table
	 * @param bodyDetails
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected void addTableBodyContentAndPersistTable(final BaseTable table, final List<List<TableBodyColumnDetails>> bodyDetails)
			throws IOException, PdfUtilException {
		for (final List<TableBodyColumnDetails> rowDetails : bodyDetails) {
			addTableRowContent(table, rowDetails);
		}
		persistTableContent(table);
	}

	/**
	 * This method draw the content table and return the exactly Y point of the end of the table
	 * @param table
	 * @return
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected float persistTableContent(final BaseTable table) throws IOException, PdfUtilException {
		if (table == null) {
			throw new PdfUtilException(PdfUtilExceptionsEnumeration.nullBaseTable);
		}
		// draw table content and return last y point on the pdf page
		return table.draw();
	}

	/**
	 * Add row to the give BaseTable
	 *
	 * @param table
	 * @param rowDetails
	 * @throws PdfUtilException
	 */
	protected void addTableRowContent(final BaseTable table, final List<TableBodyColumnDetails> rowDetails)
			throws PdfUtilException {
		if (table == null) {
			throw new PdfUtilException(PdfUtilExceptionsEnumeration.nullBaseTable);
		}
		inserTableRowContent(table, rowDetails, DEFAULT_TABLE_ROW_HEIGHT);
	}

	/**
	 * Add row to the give BaseTable specifying a row height
	 *
	 * @param table
	 * @param rowDetails
	 * @param rowheight
	 * @throws PdfUtilException
	 */
	protected void addTableRowContent(final BaseTable table, final List<TableBodyColumnDetails> rowDetails,
			final float rowheight) throws PdfUtilException {
		if (table == null) {
			throw new PdfUtilException(PdfUtilExceptionsEnumeration.nullBaseTable);
		}
		inserTableRowContent(table, rowDetails, rowheight);
	}

	/**
	 * Add content to the current BaseTale
	 * @param table
	 * @param rowDetails
	 * @param rowHeight
	 */
	private void inserTableRowContent(final BaseTable table, final List<TableBodyColumnDetails> rowDetails,
			final float rowHeight) {
		final Row<PDPage> row = table.createRow(rowHeight);
		for (final TableBodyColumnDetails columnDetails : rowDetails) {
			addColumnDetails(row, columnDetails);
		}
	}

	/**
	 * Add content to the current BaseTale
	 * @param row
	 * @param columnDetails
	 */
	private void addColumnDetails(final Row<PDPage> row, final TableBodyColumnDetails columnDetails) {
		row.createCellFormat(columnDetails.getColumnSize(), columnDetails.getColumnContent(), columnDetails.getAlign(),
				columnDetails.getFont().getFontSize(), columnDetails.getFont().getFontStyle(),
				columnDetails.getFillColor(), columnDetails.getCellBorders());
	}

	/**
	 * This function add default GPPND signatures (header and footer) and return the final PDF document as Array byte
	 *
	 * @param document The PDF document
	 * @return
	 * @throws IOException
	 * @throws PdfUtilException
	 */
	protected byte[] saveAsByteArray(final PDDocument document)
			throws IOException, PdfUtilException {
		if (document == null) {
			throw new PdfUtilException(PdfUtilExceptionsEnumeration.nullPdfDocument);
		}
		final ByteArrayOutputStream pdfFile = new ByteArrayOutputStream();
		document.save(pdfFile);
		document.close();
		return pdfFile.toByteArray();
	}

}
