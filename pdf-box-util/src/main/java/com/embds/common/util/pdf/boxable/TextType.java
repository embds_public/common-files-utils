package com.embds.common.util.pdf.boxable;

/**
 *
 */
public enum TextType {
	HIGHLIGHT, UNDERLINE, SQUIGGLY, STRIKEOUT;
}
