package com.embds.common.util.pdf.beans;

import java.awt.Color;

import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.embds.common.util.pdf.boxable.CellBorders;
import com.embds.common.util.pdf.boxable.HorizontalAlignment;
import com.embds.common.util.pdf.boxable.LineStyle;


/**
 * This class contains the details to draw on a PDF table
 *
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class TableBodyColumnDetails {

	private final String columnContent;
	private final float columnSize;
	private HorizontalAlignment align;
	private Font font;
	private Color fillColor;
	private CellBorders cellBorders;

	private TableBodyColumnDetails(final String columnContent, final float columnSize) {
		this.columnSize = columnSize;
		this.columnContent = columnContent;
	}

	/**
	 * @return the columnContent
	 */
	public String getColumnContent() {
		return columnContent;
	}

	/**
	 * @return the columnSize
	 */
	public float getColumnSize() {
		return columnSize;
	}

	/**
	 * @return the align
	 */
	public HorizontalAlignment getAlign() {
		return align;
	}

	/**
	 * @return the font
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * @return the fillColor
	 */
	public Color getFillColor() {
		return fillColor;
	}

	/**
	 * @return the cellBorders
	 */
	public CellBorders getCellBorders() {
		return cellBorders;
	}

	public static class Builder {

		private final String columnContent;
		private final float columnSize;
		private HorizontalAlignment align = HorizontalAlignment.CENTER;
		private Font font = new Font(PDType1Font.HELVETICA, 6);
		private Color fillColor = Color.WHITE;
		private final LineStyle defaultBorder = new LineStyle(Color.BLACK, 0.3f);
		private CellBorders cellBorders = new CellBorders(defaultBorder, defaultBorder, defaultBorder, defaultBorder);

		public Builder(final String columnContent, final float columnSize) {
			this.columnContent = columnContent;
			this.columnSize = columnSize;
		}

		/**
		 * Set the Horizontal Alignment
		 * @param align Default : CENTER
		 * @return
		 */
		public Builder alignment(final HorizontalAlignment align) {
			this.align = align;
			return this;
		}

		/**
		 * Set the Horizontal Alignment
		 * fontSize Default : 6
		 * font Default : HELVETICA
		 * @return
		 */
		public Builder font(final Font font) {
			this.font = font;
			return this;
		}

		/**
		 * Set the fill Color
		 * @param fillColor Default : WHITE
		 * @return
		 */
		public Builder fillColor(final Color fillColor) {
			this.fillColor = fillColor;
			return this;
		}

		/**
		 * Set the cellBorders style
		 * @param cellBorders
		 * @return
		 */
		public Builder cellBorders(final CellBorders cellBorders) {
			this.cellBorders = cellBorders;
			return this;
		}

		public TableBodyColumnDetails build() {
			final TableBodyColumnDetails details = new TableBodyColumnDetails(this.columnContent, this.columnSize);

			details.align = this.align;
			details.font = this.font;
			details.fillColor = this.fillColor;
			details.cellBorders = this.cellBorders;

			return details;
		}

	}

}
