package com.embds.common.util.pdf.exceptions.enums;

import com.embds.common.util.domain.exception.IErrorElement;

/**
 *
 * @author 
 *
 */
public enum PdfUtilExceptionsEnumeration implements IErrorElement {

	nullPdfDocument("NULL_PDF_DOCUMENT", "The PDDocument can't be null!"), nullBaseTable("NULL_PDF_BASETABLE",
			"The BaseTable can't be null!"), nullCurrentPdfDocument("NULL_CURRENT_PDF_DOCUMENT",
					"You must call startDocumentWithTable() before call this method!"), nullCurrentBaseTableDocument(
							"NULL_CURRENT_BASETABLE_DOCUMENT",
							"You must call startDocumentWithTable() before call this method!");

	private final String SECTION_CODE = "PDF.UTIL.";

	private String errorCode;
	private String errorMessage;

	private PdfUtilExceptionsEnumeration(final String errorCode, final String errorMessage) {
		this.errorCode = SECTION_CODE + errorCode;
		this.errorMessage = errorMessage;
	}

	@Override
	public String getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String getErrorMessage() {
		return this.errorMessage;
	}

}
