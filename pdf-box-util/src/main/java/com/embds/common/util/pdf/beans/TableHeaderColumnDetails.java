package com.embds.common.util.pdf.beans;

import java.awt.Color;

import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.embds.common.util.pdf.boxable.CellBorders;
import com.embds.common.util.pdf.boxable.HorizontalAlignment;
import com.embds.common.util.pdf.boxable.LineStyle;

/**
 * This class contains the details to draw on a PDF table
 *
 * @author 
 *
 */
public class TableHeaderColumnDetails {

	private final String columnContent;
	private final float columnSize;
	private HorizontalAlignment align;
	private Font font;
	private Color textColor;
	private Color fillColor;
	private float paddingTop;
	private CellBorders cellBorders;

	private TableHeaderColumnDetails(final String columnContent, final float columnSize) {
		this.columnSize = columnSize;
		this.columnContent = columnContent;
	}

	/**
	 * @return the columnContent
	 */
	public String getColumnContent() {
		return columnContent;
	}

	/**
	 * @return the columnSize
	 */
	public float getColumnSize() {
		return columnSize;
	}

	/**
	 * @return the align
	 */
	public HorizontalAlignment getAlign() {
		return align;
	}

	/**
	 * @return the font
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * @return the textColor
	 */
	public Color getTextColor() {
		return textColor;
	}

	/**
	 * @return the fillColor
	 */
	public Color getFillColor() {
		return fillColor;
	}

	/**
	 * @return the paddingTop
	 */
	public float getPaddingTop() {
		return paddingTop;
	}

	/**
	 * @return the cellBorders
	 */
	public CellBorders getCellBorders() {
		return cellBorders;
	}

	public static class Builder {

		private final String columnContent;
		private final float columnSize;
		private HorizontalAlignment align = HorizontalAlignment.CENTER;
		private Font font = new Font(PDType1Font.HELVETICA, 6);
		private Color textColor = Color.BLACK;
		private Color fillColor = Color.WHITE;
		private float paddingTop = 10;
		private final LineStyle defaultBorder = new LineStyle(Color.BLACK, 1f);
		private CellBorders cellBorders = new CellBorders(defaultBorder, defaultBorder, defaultBorder, defaultBorder);

		public Builder(final String columnContent, final float columnSize) {
			this.columnContent = columnContent;
			this.columnSize = columnSize;
		}

		/**
		 * Set the Horizontal Alignment
		 * @param align Default : CENTER
		 * @return
		 */
		public Builder alignment(final HorizontalAlignment align) {
			this.align = align;
			return this;
		}

		/**
		 * Set the Horizontal Alignment
		 * @param fontSize Default : 6
		 * @param font Default : HELVETICA
		 * @param textColor Default : BLACK
		 * @return
		 */
		public Builder font(final Font font, final Color textColor) {
			this.font = font;
			this.textColor = textColor;
			return this;
		}

		/**
		 * Set the fill Color
		 * @param fillColor Default : WHITE
		 * @return
		 */
		public Builder fillColor(final Color fillColor) {
			this.fillColor = fillColor;
			return this;
		}

		/**
		 * Set the padding Top
		 * @param paddingTop Default : 10
		 * @return
		 */
		public Builder paddingTop(final float paddingTop) {
			this.paddingTop = paddingTop;
			return this;
		}

		/**
		 * Set the cellBorders style
		 * @param cellBorders
		 * @return
		 */
		public Builder cellBorders(final CellBorders cellBorders) {
			this.cellBorders = cellBorders;
			return this;
		}

		public TableHeaderColumnDetails build() {
			final TableHeaderColumnDetails details = new TableHeaderColumnDetails(this.columnContent, this.columnSize);

			details.align = this.align;
			details.font = this.font;
			details.textColor = this.textColor;
			details.fillColor = this.fillColor;
			details.paddingTop = this.paddingTop;
			details.cellBorders = this.cellBorders;

			return details;
		}

	}

}
