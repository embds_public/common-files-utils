package com.embds.common.util.pdf.beans;

import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class Font {
	private final PDType1Font fontStyle;
	private final float fontSize;

	public Font(final PDType1Font fontStyle, final float fontSize) {
		this.fontStyle = fontStyle;
		this.fontSize = fontSize;
	}

	/**
	 * @return the font
	 */
	public PDType1Font getFontStyle() {
		return fontStyle;
	}

	/**
	 * @return the fontSize
	 */
	public float getFontSize() {
		return fontSize;
	}

}
