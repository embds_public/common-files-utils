package com.embds.common.util.pdf.boxable;

public interface WrappingFunction {

	String[] getLines(String text);
}
