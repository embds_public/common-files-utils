package com.embds.common.util.pdf.boxable;

/**
 * This class it's useful for define Style line of borders of a cell
 *
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class CellBorders {

	private final LineStyle topBorder;
	private final LineStyle bottomBorder;
	private final LineStyle leftBorder;
	private final LineStyle rightBorder;

	/**
	 * Create the borders line styles for a Cell
	 *
	 * @param topBorder
	 * @param bottomBorder
	 * @param leftBorder
	 * @param rightBorder
	 */
	public CellBorders(final LineStyle topBorder, final LineStyle bottomBorder, final LineStyle leftBorder,
			final LineStyle rightBorder) {
		this.topBorder = topBorder;
		this.bottomBorder = bottomBorder;
		this.leftBorder = leftBorder;
		this.rightBorder = rightBorder;
	}

	/**
	 * @return the topBorder
	 */
	public LineStyle getTopBorder() {
		return topBorder;
	}

	/**
	 * @return the bottomBorder
	 */
	public LineStyle getBottomBorder() {
		return bottomBorder;
	}

	/**
	 * @return the leftBorder
	 */
	public LineStyle getLeftBorder() {
		return leftBorder;
	}

	/**
	 * @return the rightBorder
	 */
	public LineStyle getRightBorder() {
		return rightBorder;
	}

}
