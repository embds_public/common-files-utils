package com.embds.common.util.pdf.exceptions;

import com.embds.common.util.domain.exception.CommonUtilException;
import com.embds.common.util.domain.exception.IErrorElement;

public class PdfUtilException extends CommonUtilException {

	private static final long serialVersionUID = -9058587781541798851L;

	public PdfUtilException(final IErrorElement error, final Exception exception) {
		super(error, exception);
	}

	public PdfUtilException(final IErrorElement error, final Object... data) {
		super(error, data);
	}

	public PdfUtilException(final IErrorElement error, final Exception exception, final Object... data) {
		super(error, exception, data);
	}

	public PdfUtilException(final IErrorElement error) {
		super(error);
	}

}
