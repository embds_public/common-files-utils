package com.embds.common.util.pdf.boxable;

import org.apache.pdfbox.pdmodel.PDPage;

public class ImageCell<T extends PDPage> extends Cell<T> {

	private Image img;

	ImageCell(final Row<T> row, final float width, final Image image, final boolean isCalculated) {
		super(row, width, null, isCalculated);
		this.img = image;
		if (image.getWidth() > getInnerWidth()) {
			scaleToFit();
		}
	}

	@SuppressWarnings("deprecation")
	public void scaleToFit() {
		img = img.scale(getInnerWidth());
	}

	ImageCell(final Row<T> row, final float width, final Image image, final boolean isCalculated,
			final HorizontalAlignment align, final VerticalAlignment valign) {
		super(row, width, null, isCalculated, align, valign);
		this.img = image;
		if (image.getWidth() > getInnerWidth()) {
			scaleToFit();
		}
	}

	@Override
	public float getTextHeight() {
		return img.getHeight();
	}

	@Override
	public float getHorizontalFreeSpace() {
		return getInnerWidth() - img.getWidth();
	}

	@Override
	public float getVerticalFreeSpace() {
		return getInnerHeight() - img.getHeight();
	}

	/**
	 * <p>
	 * Method which retrieve {@link Image}
	 * </p>
	 *
	 * @return {@link Image}
	 */
	public Image getImage() {
		return img;
	}
}
