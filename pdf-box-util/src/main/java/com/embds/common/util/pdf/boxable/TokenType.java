package com.embds.common.util.pdf.boxable;

public enum TokenType {
	TEXT, POSSIBLE_WRAP_POINT, WRAP_POINT, OPEN_TAG, CLOSE_TAG, PADDING, BULLET, ORDERING
}
