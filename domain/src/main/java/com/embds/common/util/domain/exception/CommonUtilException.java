package com.embds.common.util.domain.exception;


public class CommonUtilException extends Exception {

	private static final long serialVersionUID = 2824054398056540569L;

	private final String code;

	private Object[] data;

	/**
	 * Constructor only used for inherit classes!
	 */
	protected CommonUtilException() {
		this.code = "defaultConstructorCommonException";
	}

	protected CommonUtilException(final IErrorElement exceptionValues) {
		super(exceptionValues.getErrorMessage());
		this.code = exceptionValues.getErrorCode();
	}

	protected CommonUtilException(final IErrorElement exceptionValues, final Exception exception) {
		super(exceptionValues.getErrorMessage(), exception);
		this.code = exceptionValues.getErrorCode();
	}

	/**
	 * Create an exception with give values
	 * 
	 * @param exceptionValues The IErrorElement containing the code and message
	 * @param data The data of the exception
	 */
	protected CommonUtilException(final IErrorElement exceptionValues, final Object... data) { 
		super(mergeDataIntoMessage(exceptionValues.getErrorMessage(), data));
		this.code = exceptionValues.getErrorCode();
		this.data = data;
	}

	protected CommonUtilException(final IErrorElement exceptionValues, final Exception exception, final Object... data) {
		super(exceptionValues.getErrorMessage(), exception);
		this.code = exceptionValues.getErrorCode();
		this.data = data;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the data
	 */
	public Object[] getData() {
		return data;
	}

	private static String mergeDataIntoMessage(final String message, final Object... data) {
		return String.format(message, data);
	}

}
