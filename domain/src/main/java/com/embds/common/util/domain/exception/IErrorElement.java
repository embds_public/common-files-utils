package com.embds.common.util.domain.exception;


public interface IErrorElement {

	/**
	 * Return the error code
	 * @return String The code
	 */
	public String getErrorCode();

	/**
	 * Return the error message
	 * @return String The message
	 */
	public String getErrorMessage();
}
