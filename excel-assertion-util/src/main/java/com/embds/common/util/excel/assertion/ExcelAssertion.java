package com.embds.common.util.excel.assertion;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.embds.common.util.excel.ExcelHelper;
import com.embds.common.util.excel.assertion.exceptions.ExcelAssertionException;
import com.embds.common.util.excel.assertion.exceptions.enumerations.ExcelAssertionExceptionsEnumeration;

/**
 * This class help you to assert the values of an excel file
 * 
 * @since
 * <pre>	
 * &#064;Test
 * public void test_allUsersExcelFile() throws ExcelAssertionException {
 *	final byte[] generatedFile = getYourOwnGeneratedFile();
 *	final File expectedFile = getYourOwnExpectedFile();
 *
 * 	final ExcelAssertion excelAssert = new ExcelAssertion(generatedFile, expectedFile);
 *	Assert.assertTrue(excelAssert.cellHasSameValue(0, 0, 0));
 *	Assert.assertTrue(excelAssert.sheetHaveSameValuesFromLine(0, 1));
 * }
 * </pre>
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class ExcelAssertion extends ExcelHelper {

	private final Workbook actualFileWorkBook;

	private Workbook expectedFileWorkBook;

	/**
	 * This constructor create the workbook of actual and expected excel files for
	 * testing
	 * 
	 * @param actualFile
	 * @param expectedFile
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public ExcelAssertion(byte[] actualFile, File expectedFile)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		this.actualFileWorkBook = WorkbookFactory.create(new ByteArrayInputStream(actualFile));
		this.expectedFileWorkBook = WorkbookFactory.create(expectedFile);
	}

	/**
	 * This constructor create the workbook of actual excel file for testing
	 * 
	 * @param actualFile
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public ExcelAssertion(byte[] actualFile) throws EncryptedDocumentException, InvalidFormatException, IOException {
		this.actualFileWorkBook = WorkbookFactory.create(new ByteArrayInputStream(actualFile));
	}

	/**
	 * This constructor create the workbook of actual and expected excel files for
	 * testing
	 * 
	 * @param actualFile
	 * @param expectedFile
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public ExcelAssertion(File actualFile, File expectedFile)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		this.actualFileWorkBook = WorkbookFactory.create(actualFile);
		this.expectedFileWorkBook = WorkbookFactory.create(expectedFile);
	}

	/**
	 * This constructor create the workbook of actual and expected excel files for
	 * testing
	 * 
	 * @param actualFile
	 * @param expectedFile
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public ExcelAssertion(byte[] actualFile, byte[] expectedFile)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		this.actualFileWorkBook = WorkbookFactory.create(new ByteArrayInputStream(actualFile));
		this.expectedFileWorkBook = WorkbookFactory.create(new ByteArrayInputStream(expectedFile));
	}

	/**
	 * This function verify if the value of given sheet, line and column are the
	 * same on both excel files
	 * 
	 * @param sheet
	 *            The sheet to find value
	 * @param line
	 *            The Line of the record to test
	 * @param column
	 *            The column of the record to test
	 * @return True if same value
	 * @throws ExcelAssertionException
	 */
	public boolean cellHasSameValue(int sheet, int line, int column) throws ExcelAssertionException {
		if (this.expectedFileWorkBook == null) {
			throw new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.EXPECTED_FILE_REQUIRED);
		}
		Object valueOfActualFile = getCellValueNoNull(this.actualFileWorkBook, sheet, line, column);
		Object valueOfExpectedFile = getCellValueNoNull(this.expectedFileWorkBook, sheet, line, column);
		if (!valueOfActualFile.equals(valueOfExpectedFile)) {
			throw new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.FAILED_CELL_ASSERTION,
					valueOfExpectedFile, valueOfActualFile);
		}
		return true;
	}

	/**
	 * This function verify if the given value is the same on actual excel file
	 * (generated file) on the given sheet index, line number and column number
	 * 
	 * @param sheet
	 *            The sheet to find value
	 * @param line
	 *            The Line of the record to test
	 * @param column
	 *            The column of the record to test
	 * @param expectedValue
	 *            The value who you expect to have of the cell
	 * @return true if value is the same
	 * @throws ExcelAssertionException
	 */
	public boolean cellHasValue(int sheet, int line, int column, Object expectedValue) throws ExcelAssertionException {
		Object valueOfActualFile = getCellValue(this.actualFileWorkBook, sheet, line, column);
		if (!valueOfActualFile.equals(expectedValue)) {
			throw new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.FAILED_VALUE_ASSERTION, expectedValue,
					valueOfActualFile);
		}
		return true;
	}

	/**
	 * This function verify, for the sheet index give, if the values of all edited
	 * cells of the actual excel file (generated file) are the same on expected file
	 * 
	 * <p>
	 * <b>ATTENTION: This verification only works on one side! It means, only values
	 * of edited cells of the actual excel file are checked on the expected excel
	 * file</b>
	 * 
	 * @param sheetIndex
	 *            The sheet index to verify
	 * @return True if the values are the same on both excel files
	 * @throws ExcelAssertionException
	 */
	public boolean sheetHaveSameValues(int sheetIndex) throws ExcelAssertionException {
		Sheet actualFileSheet = this.actualFileWorkBook.getSheetAt(sheetIndex);
		for (final Row row : actualFileSheet) {
			for (Cell cell : row) {
				Object actualFileCellValue = getCellValueNoNull(cell);
				Object expectedFileCellValue = getCellValueNoNull(expectedFileWorkBook, sheetIndex, row.getRowNum(),
						cell.getColumnIndex());
				if (!actualFileCellValue.equals(expectedFileCellValue)) {
					throw new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.FAILED_VALUE_ROW_ASSERTION,
							row.getRowNum(), cell.getColumnIndex(), expectedFileCellValue, actualFileCellValue);
				}
			}
		}
		return true;
	}

	/**
	 * Check if the sheet has the same values starting at provided index line
	 * <p>
	 * <b>Note :</b> The expected file is used has reference for the test!
	 * 
	 * @param sheetindex
	 *            The sheet index of the sheet to test
	 * @param indexLineToStart
	 *            The index line to start verification
	 * @return
	 * @throws ExcelAssertionException
	 */
	public boolean sheetHaveSameValuesFromLine(int sheetIndex, int indexLineToStart) throws ExcelAssertionException {
		int lastRowIndex = this.expectedFileWorkBook.getSheetAt(sheetIndex).getLastRowNum();
		return sheetHaveSameValuesBetweenLines(sheetIndex, indexLineToStart, lastRowIndex);
	}

	/**
	 * Check if sheet have same values on same provided index line
	 * 
	 * @param sheetIndex
	 * @param indexlineToStart
	 * @return
	 * @throws ExcelAssertionException
	 */
	public boolean sheetHaveSameValuesOnSameLine(int sheetIndex, int indexline) throws ExcelAssertionException {
		return sheetHaveSameValuesBetweenLines(sheetIndex, indexline, indexline);
	}

	/**
	 * Check if sheet has the same values between the index line to start and index
	 * line to Stop
	 * 
	 * @param sheetIndex
	 * @param indexlineToStart
	 * @return
	 * @throws ExcelAssertionException
	 */
	public boolean sheetHaveSameValuesBetweenLines(int sheetIndex, int indexLineToStart, int indexlineToStop)
			throws ExcelAssertionException {
		if (indexLineToStart > indexlineToStop) {
			throw new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.INVALID_PROVIDED_INDEX_ROWS,
					indexLineToStart, indexlineToStop);
		}
		for (int rowIndex = indexLineToStart; rowIndex <= indexlineToStop; rowIndex++) {
			Row currentRow = this.expectedFileWorkBook.getSheetAt(sheetIndex).getRow(rowIndex);
			if (currentRow != null) {
				int lastColumnIndex = currentRow.getLastCellNum();
				for (int columnIndex = 0; columnIndex < lastColumnIndex; columnIndex++) {
					cellHasSameValue(sheetIndex, rowIndex, columnIndex);
				}
			}
		}
		return true;
	}

}
