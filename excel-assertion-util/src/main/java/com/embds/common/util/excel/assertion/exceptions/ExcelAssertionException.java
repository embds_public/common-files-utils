package com.embds.common.util.excel.assertion.exceptions;

import com.embds.common.util.domain.exception.CommonUtilException;
import com.embds.common.util.domain.exception.IErrorElement;

/**
 * The exception for AssertExcelFile class
 * 
 * @author  Eduardo BORGES DA SILVA
 *
 */
public class ExcelAssertionException extends CommonUtilException {

	private static final long serialVersionUID = 7041218271080578846L;

	/**
	 * Create a {@link ExcelAssertionException} exception
	 * 
	 * @param exceptionError
	 *            The enumeration with the error details
	 */
	public ExcelAssertionException(final IErrorElement exceptionError) {
		super(exceptionError);
	}

	public ExcelAssertionException(final IErrorElement exceptionError, final Object... data) {
		super(exceptionError, data);
	}
}
