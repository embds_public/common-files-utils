package com.embds.common.util.excel.assertion.exceptions.enumerations;

import com.embds.common.util.domain.exception.IErrorElement;

public enum ExcelAssertionExceptionsEnumeration implements IErrorElement {

	EXPECTED_FILE_REQUIRED("EXPECTED_FILE_REQUIRED","You should set the expected excel file!"),
	FAILED_CELL_ASSERTION("FAILED_CELL_ASSERTION", "The expected value was '%1$s' but the actual value is '%2$s'"),
	FAILED_VALUE_ASSERTION("FAILED_VALUE_ASSERTION", "The expected value was '%1$s' but the actual value is '%2$s'"),
	FAILED_VALUE_ROW_ASSERTION("UNEXPECTED_VALUE_ASSERTION", "The expected value for row %1$s and column %2$s was '%3$s' but the actual value is '%4$s'"),
	INVALID_PROVIDED_INDEX_ROWS("INVALID_PROVIDED_ROWS_INDEX","The start line index can't be less than the end line index! Actually, the start line index is %1$s and the stop line index is %2$s");

	private final String SECTION_CODE = "EXCEL.ASSERTION.UTIL.";

	private String errorCode;
	private String errorMessage;

	private ExcelAssertionExceptionsEnumeration(final String code, final String message) {
		this.errorCode = SECTION_CODE + code;
		this.errorMessage = message;
	}

	@Override
	public String getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String getErrorMessage() {
		return this.errorMessage;
	}

}
