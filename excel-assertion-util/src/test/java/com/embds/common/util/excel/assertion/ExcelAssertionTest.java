package com.embds.common.util.excel.assertion;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.embds.common.util.excel.assertion.ExcelAssertion;
import com.embds.common.util.excel.assertion.exceptions.ExcelAssertionException;
import com.embds.common.util.excel.assertion.exceptions.enumerations.ExcelAssertionExceptionsEnumeration;
import com.embds.common.util.excel.assertion.exceptions.matcher.ExcelAssertionExceptionMatcher;


public class ExcelAssertionTest extends ExcelAssertionTestHelper{
	
	private final static int SHEET_INDEX = 0;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testBetweenLinesExcelValues() throws EncryptedDocumentException, InvalidFormatException, IOException, ExcelAssertionException{
		ExcelAssertion excelAssertion = new ExcelAssertion(getFileTestBetweenLines1(), getFileTestBetweenLines2());
		Assert.assertTrue(excelAssertion.sheetHaveSameValuesBetweenLines(SHEET_INDEX, 2, 2));
		Assert.assertTrue(excelAssertion.sheetHaveSameValuesBetweenLines(SHEET_INDEX, 4, 5));
	}
	
	@Test
	public void testLineExcelValues() throws EncryptedDocumentException, InvalidFormatException, IOException, ExcelAssertionException{
		ExcelAssertion excelAssertion = new ExcelAssertion(getFileTestByLine1(), getFileTestByLine2());
		Assert.assertTrue(excelAssertion.sheetHaveSameValuesOnSameLine(SHEET_INDEX, 5));
	}
	
	@Test
	public void testFromEspecificLineExcelValues() throws EncryptedDocumentException, InvalidFormatException, IOException, ExcelAssertionException{
		ExcelAssertion excelAssertion = new ExcelAssertion(getFileTestFromLine1(), getFileTestFromLine2());
		Assert.assertTrue(excelAssertion.sheetHaveSameValuesFromLine(SHEET_INDEX, 1));
	}
	
	@Test
	public void testFileExcelHasSameValues() throws EncryptedDocumentException, InvalidFormatException, IOException, ExcelAssertionException {
		ExcelAssertion excelAssertion = new ExcelAssertion(getFileTestByLine1(), getFileTestByLine1());
		Assert.assertTrue(excelAssertion.sheetHaveSameValues(SHEET_INDEX));
	}
	
	@Test
	public void testFileExcelHasDifferentValue() throws EncryptedDocumentException, InvalidFormatException, IOException, ExcelAssertionException {
		exception.expect(ExcelAssertionExceptionMatcher.hasException(new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.FAILED_VALUE_ROW_ASSERTION, 0, 3, "", "column tested 2")));
		ExcelAssertion excelAssertion = new ExcelAssertion(getFileTestSomeNull1(), getFileTestSomeNull2());
		excelAssertion.sheetHaveSameValues(SHEET_INDEX);
	}

}
