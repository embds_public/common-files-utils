package com.embds.common.util.excel.assertion;

import java.io.File;

/**
 * This class contains utility functions to get files for test assertion methods
 * 
 * <p><b>ATTENTION : </b> <br>
 * The excel test files should be on resources folder inside a package with same name of this class and inside a package named files
 *
 */
public class ExcelAssertionTestHelper {

	private final Class<? extends ExcelAssertionTestHelper> c = this.getClass();
	
	private final static String MAIN_PATH_EXCEL_FILES_TEST = "src/test/resources/";
	private final static String LOCATION_FOLDER_EXCEL_FILES_TEST = "/files/";

	private final static String FILE_NAME_TEST_BETWEEN_LINES_1 = "test_between_lines_1.xlsx";
	private final static String FILE_NAME_TEST_BETWEEN_LINES_2 = "test_between_lines_2.xlsx";
	
	private final static String FILE_NAME_TEST_BY_LINE_1 = "test_by_line_1.xlsx";
	private final static String FILE_NAME_TEST_BY_LINE_2 = "test_by_line_2.xlsx";
	
	private final static String FILE_NAME_TEST_FROM_LINE_1 = "test_from_line_to_end_1.xlsx";
	private final static String FILE_NAME_TEST_FROM_LINE_2 = "test_from_line_to_end_2.xlsx";

	private final static String FILE_NAME_TEST_SOME_NULL_1 = "test_some_null_1.xlsx";
	private final static String FILE_NAME_TEST_SOME_NULL_2 = "test_some_null_2.xlsx";
		
	protected File getFileTestBetweenLines1(){
		return new File(getCompleteFileName(FILE_NAME_TEST_BETWEEN_LINES_1));
	}
	
	protected File getFileTestBetweenLines2(){
		return new File(getCompleteFileName(FILE_NAME_TEST_BETWEEN_LINES_2));
	}
	
	protected File getFileTestByLine1(){
		return new File(getCompleteFileName(FILE_NAME_TEST_BY_LINE_1));
	}
	
	protected File getFileTestByLine2(){
		return new File(getCompleteFileName(FILE_NAME_TEST_BY_LINE_2));
	}
	
	protected File getFileTestFromLine1(){
		return new File(getCompleteFileName(FILE_NAME_TEST_FROM_LINE_1));
	}
	
	protected File getFileTestFromLine2(){
		return new File(getCompleteFileName(FILE_NAME_TEST_FROM_LINE_2));
	}
	
	protected File getFileTestSomeNull1(){
		return new File(getCompleteFileName(FILE_NAME_TEST_SOME_NULL_1));
	}
	
	protected File getFileTestSomeNull2(){
		return new File(getCompleteFileName(FILE_NAME_TEST_SOME_NULL_2));
	}
	
	/**
	 * Return the string complete path name file
	 * 
	 * @param fileName The name of file which we want get
	 * @return
	 */
	private String getCompleteFileName(String fileName){
		return MAIN_PATH_EXCEL_FILES_TEST + getPackageLocation() + LOCATION_FOLDER_EXCEL_FILES_TEST + fileName;
	}
	
	/**
	 * Get this package location and replace . by /
	 * 
	 * <p>
	 * This method was created for facility when refactor on package names : 
	 * not need to update complete location of files 
	 * @return
	 */
	private String getPackageLocation() {
		return c.getPackage().getName().replace(".", "/");
	}

}
