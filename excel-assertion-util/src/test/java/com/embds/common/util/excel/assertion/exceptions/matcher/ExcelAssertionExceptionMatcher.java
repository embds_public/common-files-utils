package com.embds.common.util.excel.assertion.exceptions.matcher;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import com.embds.common.util.excel.assertion.exceptions.ExcelAssertionException;

/**
 * This class is to be used on jUnit tests and should be used to test if GppndException code error is ok
 *
 * @since
 * <pre>
 * &#064;Rule public ExpectedException exception = ExpectedException.none();
 * 
 * &#064;Test
 * public void sampleTest() throws ExcelAssertionException {
 *		// Testing the exception error code only
 *		exception.expect(ExcelAssertionExceptionMatcher.hasCode(ExcelAssertionExceptionsEnumeration.EXPECTED_FILE_REQUIRED.getErrorCode()));
 *		// Testing complete exception looking at error code, message and data
 *		exception.expect(ExcelAssertionExceptionMatcher.hasException(
 *				new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.EXPECTED_FILE_REQUIRED, "One or more parameters")));
 *		throw new ExcelAssertionException(ExcelAssertionExceptionsEnumeration.EXPECTED_FILE_REQUIRED, "One or more parameters");
 *	}
 * </pre>
 *
 * @see <a href="https://github.com/mike-ensor/custom-exception-testing/commit/c555fdcd36d4814d294fbdf1f9094363d54c817a">
 * https://github.com/mike-ensor/custom-exception-testing/commit/c555fdcd36d4814d294fbdf1f9094363d54c817a</a>
 *
 * @author Eduardo BORGES DA SILVA
 */
public class ExcelAssertionExceptionMatcher extends TypeSafeMatcher<ExcelAssertionException> {

	private final static String INFO_PHRASE = " was not found instead of ";
	private final static String INFO_CODE = " Code : ";
	private final static String INFO_MESSAGE = " Message : ";
	private final static String INFO_DATA = " Data : ";
	private final static String BREAK_PHRASE = "\n";

	private String foundErrorCode;
	private String foundErrorMessage;
	private Object[] foundErrorData;
	private final String expectedErrorCode;
	private String expectedErrorMessage;
	private Object[] expectedErrorData;

	private ExcelAssertionExceptionMatcher(final String errorCode) {
		this.expectedErrorCode = errorCode;
	}
	
	private ExcelAssertionExceptionMatcher(ExcelAssertionException expectedException) {
		this.expectedErrorCode = expectedException.getCode();
		this.expectedErrorMessage = expectedException.getMessage(); 
		this.expectedErrorData = expectedException.getData();
	}

	/**
	 * Check if the exception code it's the same of the provided expectedErrorCode
	 * 
	 * @param expectedErrorCode The expected error code
	 * @return ExcelAssertionExceptionMatcher
	 */
	public static ExcelAssertionExceptionMatcher hasCode(final String expectedErrorCode) {
		return new ExcelAssertionExceptionMatcher(expectedErrorCode);
	}

	/**
	 * Check if the exception values are the same of the provided expectedException
	 * <p>
	 * <pre>
	 * - Check if exception error code is the same of the expectedException
	 * - If a message exists on the expectedException, will check if is the same of exception message
	 * - If data exists on the expectedException, will check if is the same of exception data. 
	 * 	<b>The order of the elements on the data will be important! (data should be on same order!)</b>
	 * </pre>
	 * @param expectedException The expected exception
	 * @return ExcelAssertionExceptionMatcher
	 */
	public static ExcelAssertionExceptionMatcher hasException(final ExcelAssertionException expectedException) {
		return new ExcelAssertionExceptionMatcher(expectedException);
	}

	@Override
	public void describeTo(final Description description) {
		if (!checkEquality(this.expectedErrorCode, this.foundErrorCode)) {
			description.appendText(BREAK_PHRASE);
			description.appendText(INFO_CODE);
			description.appendValue(this.foundErrorCode).appendText(INFO_PHRASE)
					.appendValue(this.expectedErrorCode);
		}
		if (this.expectedErrorMessage != null) {
			if (!checkEquality(this.expectedErrorMessage, this.foundErrorMessage)) {
				description.appendText(BREAK_PHRASE);
				description.appendText(INFO_MESSAGE);
				description.appendValue(this.foundErrorMessage).appendText(INFO_PHRASE).appendValue(this.expectedErrorMessage);
			}
		}
		if (this.expectedErrorData != null) {
			if (!checkEquality(this.expectedErrorData, this.foundErrorData)) {
				description.appendText(BREAK_PHRASE);
				description.appendText(INFO_DATA);
				description.appendValue(this.foundErrorData).appendText(INFO_PHRASE).appendValue(this.expectedErrorData);
			}
		}
	}

	@Override
	protected boolean matchesSafely(final ExcelAssertionException exception) {
		boolean validException;
		this.foundErrorCode = exception.getCode();
		validException = checkEquality(this.expectedErrorCode, this.foundErrorCode);
		if (this.expectedErrorMessage != null) {
			this.foundErrorMessage = exception.getMessage();
			if (validException) {
				validException = checkEquality(this.expectedErrorMessage, this.foundErrorMessage);
			}
		}
		if (this.expectedErrorData != null) {
			this.foundErrorData = exception.getData();
			if (validException) {
				validException = checkEquality(this.expectedErrorData, this.foundErrorData);
			}
		}
		return validException;
	}
	
	/**
	 * This method compare the expected and found values and return true if are they equals
	 * 
	 * @param expected The expected value
	 * @param found The found value
	 * @return <b>true</b> if the values are equals, <b>false</b> instead!
	 */
	private boolean checkEquality(String expected, String found) {
		boolean equal = false;
		if (expected == null && found == null) {
			equal = true;
		}else if(expected != null && found != null) {
			equal = expected.equals(found);
		}
		return equal;
	}
	
	/**
	 * This method compare the values of expected and found Object[] data
	 * 
	 * @param expected The expected Object[] data
	 * @param found The found Object[] data
	 * @return <b>true</b> if all data equals and on same order, <b>false</b> instead!
	 */
	private boolean checkEquality(Object[] expected, Object[] found) {
		boolean equal = false;
		if (expected == null && found == null) {
			equal = true;
		}
		if (expected.length == found.length) {
			for(int i = 0;i<expected.length;i++) {
				equal = checkEquality(expected[i].toString(), found[i].toString());
				if(!equal) {
					break;
				}
			}
		}
		return equal;
	}
}
